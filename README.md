# Markup for Mtb Forex Site

## First setup

Just run `bower i` and `npm i` inside project root.

## Basic cli commands to develop 

- Start development server which allows you to work under src file at `./app` directory.
```bash
./node_modules/.bin/gulp serve 
```

- Build project. After that command you get prepared files at `./dist` directory. 
This command will worried by itself about clean prev build and creating of the new.
```bash 
./node_modules/.bin/gulp 
```