(function($, window) {
	'use strict';
  var page = {};
  page.$header = null;
  page.$headerSubmenu = null;

  page.init = function (headerSelector, headerSubmenuSelector) {
    this.$header = $(headerSelector);
    this.$headerSubmenu = $(headerSubmenuSelector);
  };

  page.getHeaderOffset = function () {
    return (!!this.$header && !!this.$header.length ? this.$header.outerHeight() : 0) +
      (!!this.$headerSubmenu && !!this.$headerSubmenu.length ? this.$headerSubmenu.outerHeight() : 0);
  };

  page.calculateOffsetToElem = function ($elem){
    if(!($elem instanceof jQuery)){
      console.error('`$elem` expect to be jQuery elem');
      return 0;
    }
    return $elem.offset().top - this.getHeaderOffset();
  };


  $(function(){
    page.init('.navbar-header', '.dropdown');
  });

	const $slick = $('.slick');
	function checkWidth() {
    var windowsize = $(window).width();
    if (windowsize < 768) {
      if (!$slick.hasClass('slick-initialized')) {
        $slick.slick({
          infinite: false,
          adaptiveHeight: true,
          dots: true,
          speed: 300,
          slidesToShow: 1
        });
      }
    } else {
      if ($slick.hasClass('slick-initialized')) {
        $slick.slick('unslick');
      }
    }
  }
  // Execute on load
  checkWidth();
  // Bind event listener
  $(window).resize(checkWidth);



  // multi step form with validation
  const $form = $('.form');

  $form.each(function(index, element) {
    const $sections = $(element).find('.form__step'),
      $navigation = $(element).find('.form__navigation'),
      $formErrorText = $(element).find('.form-error-text'),
      $formNavSteps = $(element).find('.form__navigation__steps');

    function navigateTo(index) {
      // current fieldset
      $sections.removeClass('current').eq(index).addClass('current');
      $formNavSteps.children().eq(index).addClass('completed');
    }

    function curIndex() {
      return $sections.index($sections.filter('.current'));
    }

    function activateButtons(index, element) {
      var $steps = $('.form__navigation__steps');
      $steps.children().removeClass('rendered').removeClass('inactive').eq(index-1).addClass('rendered');
      $steps.children().eq(index+1).addClass('rendered').addClass('inactive');
      $steps.children().eq(index).next().next().removeClass('rendered');
      $steps.children().eq(index).next().nextAll().addClass('inactive');

      if (index === element.length-1) {
        $navigation.hide();
      }
    }

    // Previous button is easy
    $formNavSteps.on('click', 'button', function() {
      var $this= $(this);

      var validate = ($this.index() === curIndex()-1) ? true : false,
          validate2 =($this.index() === curIndex()+1) ? true : false;

      if ($this.hasClass('completed') && validate2) {
        $navigation.find('[type="submit"]').eq(0).trigger('click');
        setTimeout(function() {
          if ($formErrorText.is(':hidden')) {
            navigateTo($this.index());
            activateButtons(curIndex(), $this.parents('.form').find('.form__step'));
          }
        },0);
      }

      if ($this.hasClass('completed') && validate) {
        $formErrorText.removeClass('show');
        navigateTo($this.index());
        activateButtons(curIndex(), $this.parents('.form').find('.form__step'));
      }
      return false;
    });

    // Next button goes forward if current block validates
    let listOfErrorLabels = [];
    window.Parsley.on('field:error', fieldInstance => {
      let arrErrorMsg = ParsleyUI.getErrorsMessages(fieldInstance);
      let errorMsg = arrErrorMsg.join(';');
      let fieldName = fieldInstance.$element.parents('.form__wrap').find('label').text();
      listOfErrorLabels.push(fieldName);
      listOfErrorLabels = _.union(listOfErrorLabels);
    });

    $navigation.on('click', '[type="submit"]', function(e) {
      e.preventDefault();
      listOfErrorLabels.length = 0;
      var $form = $(this).parents('.form');
      if ($(this).parents('.form').parsley({
        successClass: 'form-success',
        errorClass: 'form-error',
        classHandler: function (el) {
          return el.$element.parents('.form__wrap');
        },
        errorsWrapper: '',
        errorsContainer: function(parsleyField) {}
      }).validate({group: 'block-' + curIndex()})) {
        var $this = $(this),
            $errorField = $this.parents('.form').find('.form-error-text');
        var i = $form.serialize();
        $errorField.removeClass('show');
        $.ajax({
          url: '/_components/forms/formActions/action',
          method: 'POST',
          data: i,
          success: function(res) {
            if (res.success) {
              var redirectTo = $this.data('redirectTo');
              console.log(redirectTo, 'redirectTo');
              if(redirectTo) {
                window.location.href = redirectTo;
              } else {
                navigateTo(curIndex() + 1);
                $errorField.removeClass('show');
                // activateButtons(curIndex(), $this.parents('.form').find('.form__step'));
              }
            }
            if(res.error) {
              $errorField.text(res.error.message).show();
            }
          }
        });

      }

      // check if there are any errors
      // if yes then show a error text with list of field that are missed
      if (listOfErrorLabels.length > 0) {
        let updatedList = listOfErrorLabels.reduce((template, text, index) => {
          return template = `${template} &nbsp;<span>${text}</span>&nbsp;`;
        }, '');
        $(this).parents('.form').find('.form-error-text').addClass('show').html(`Ошибка при заполнении полей ${updatedList}`);
      } else {
        $(this).parents('.form').find('.form-error-text').removeClass('show');
      }
    });

    $sections.each((index, section) => {
      $(section).find(':input').attr('data-parsley-group', 'block-' + index);
    });

    navigateTo(0);

  });


  $('input[data-type="phone"]').inputmask({'mask': '+375-99-999-99-99'});
  $('input[data-type="numbers"]').inputmask('9{14}', {
    'placeholder' : ''
  });
  $('input[data-type="year"]').inputmask('9{4}', {
    'placeholder' : ''
  });
  $('input[data-type="month"]').inputmask('9{2}', {
    'placeholder' : ''
  });
  $('input[data-type="passport"]').inputmask('aa9{7}', {
    'placeholder' : ''
  });
  $('input[data-type="ident"]').inputmask({
    'mask':'9{7} a 9{3} aa 9',
    'placeholder' : ''
  });

  $('input.day').on('keyup', function(e) {
    var value = e.target.value;
    if (value > 31) {
      e.target.value = 31;
    }
  });
  $('input.month').on('keyup', function(e) {
    var value = e.target.value;
    if (value > 12) {
      e.target.value = 12;
    }
  });

  $('input[data-type="letters"]').on('input', function(event) {
    var inputText = $(this).val();

    var resultText = inputText.replace(/[^а-яё]/gi, '');
    $(this).val(resultText);
  });


  $(document).on('keypress', function(evt) {
    if(evt.isDefaultPrevented()) {
      // Assume that's because of maskedInput
      // See https://github.com/guillaumepotier/Parsley.js/issues/1076
      $(evt.target).trigger('input');
    }
  });

  // select
  var $formSelect = $('.form__select'),
      $targetInput = $('input[data-type="ident"]');
  $formSelect.select2();
  $formSelect.on('select2:select', function (evt) {
    var obj = $formSelect.select2('data'),
        minlength = ['18', '6'],
        selectType = +obj[0].element.getAttribute('data-select-type');

    if (!!selectType) {
      $targetInput.inputmask({
        'mask':'9{7} a 9{3} aa 9',
        'placeholder' : ''
      });
      $targetInput.attr('minlength', minlength[0]);
    } else {
      $targetInput.inputmask('remove');
      $targetInput.attr('minlength', minlength[1]);
    }
  });

  var $navbarBrandAnimate = $('.navbar-brand-animate'),
      $navItemAnimate = $('.nav-animate');

  if ($navbarBrandAnimate.length) {
    $(window).on('scroll', function() {

      var windowTop = $(window).scrollTop();
      var targetElement= $('.hero .hero__logo');
      if(targetElement.length){
        var targetElementTop = targetElement.offset().top;
        var targetElementHeight = targetElement.height();
        if (windowTop > (targetElementTop+targetElementHeight)) {
          $navbarBrandAnimate.addClass('active');
          $navItemAnimate.addClass('topanimate');
        } else {
          $navbarBrandAnimate.removeClass('active');
          $navItemAnimate.removeClass('topanimate');
        }
      }
    });
  }


  // stick when scrolling
  var $stickyBlock = $('.content__bfamily');
  // $('.content__bfamily').sticky();
  if ($stickyBlock.length) {
    var stickyTop = $stickyBlock.offset().top,
        stickyLeft = $stickyBlock.offset().left,
        stickyHeight = $stickyBlock.outerHeight(),
        leftPosition = $stickyBlock.parent().offset().left + $stickyBlock.position().left,
        number = 30;

    function scrollEvent() {
      $(window).scroll(function(){
        var limit = $('.main-footer').offset().top - stickyHeight,
            windowTop = $(window).scrollTop()+number;

        if (stickyTop < windowTop){
          $stickyBlock.css({ position: 'fixed', top: '30px'});
        } else {
          $stickyBlock.css({ position: 'static'});
        }

        if (limit < windowTop+number) {
          var diff = limit - windowTop;
          $stickyBlock.css({top: diff});
        }
      });
    }
    scrollEvent();
  }

  // position of input
  function doGetCaretPosition(oField) {
    // Initialize
    var iCaretPos = 0;
    // IE Support
    if (document.selection) {
      oField.focus();
      var oSel = document.selection.createRange ();
      oSel.moveStart ('character', -oField.value.length);
      iCaretPos = oSel.text.length;
    } else if (oField.selectionStart || oField.selectionStart == '0') {
      iCaretPos = oField.selectionStart;
    }

    return (iCaretPos);
  }


  $('input').on('keypress keydown', function(e) {
    var tabkey = 9;
    if(e.keyCode == tabkey) {
      $(this).closest('input').focus();
    }
  });

  $('.date-custom input').on('focus keypress keyup keydown', function() {
    var $this = $(this);
    switchInput.call($this[0]);
  });

  // switches between inputs
  function switchInput() {
    var $this = $(this),
        max = $this.attr('maxlength') || 2,
        currentLength = $this.val().length,
        key = event.keyCode || event.charCode,
        dataPrevLength;

    if (event.type === 'keydown') {
      $this.attr('data-prev', $this.val());
      dataPrevLength = $this.attr('data-prev').length;
    }
    if (event.type === 'keyup') {
      var position = doGetCaretPosition($this[0]);
      if (position === 0 && (key == 8 || key == 46)) {
        $this.prev('input').focus();
      }

      if (position === 0 && dataPrevLength == 0 && currentLength === 0 && (key == 8 || key == 46)) {
        $this.prev('input').focus();
      }

      if ((key !== 37 || key !== 39) && currentLength === max-- && position === 2) {
        $this.next('input').focus();
      }
    }
  }

  // custom input file
  var inputs = document.querySelectorAll( '.file-custom input[type="file"]' );
	Array.prototype.forEach.call( inputs, function( input ) {
		var label	 = input.parentNode,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e ) {
			var fileName = '',
          target = e.target,
          that = this;

      var file = target.files[0];

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'strong' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;


      if (file) {
        var r = new FileReader();
        r.onload = function(e) {
          var sizeMB = file.size/1024/1024;
          if (sizeMB.toFixed(2) > 20) {
            target.value = '';
            label.querySelector( 'strong' ).innerHTML = '';
            return false;
          }
        }
        r.readAsText(file);
      }
		});
	});

  // Test for background clip text
  Modernizr.addTest('backgroundcliptext',function(){
    var div = document.createElement('div');
    div.style.webkitBackgroundClip = 'text';
    var text = div.style.cssText.indexOf('text');
    if (text > 0) return true;
    'Webkit Moz O ms Khtml'.replace(/([A-Za-z]*)/g,function(val){
        if (val+'BackgroundClip' in div.style) return true;
    });
  });

  if (!Modernizr.backgroundcliptext) {
    $('.txt-gradient').css({background:'transparent'});
  }


  // Cache selectors
  var lastId,
      topMenu = $('.dropdown ul'),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find('a'),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var hash = $(this).attr('href');
        if(hash.indexOf('#') == 0 && hash.length > 1){
          var item = $(hash);
          if (item.length) { return item; }
        }
        return null
      }).filter(function(i, el) {return !!el;});

  scrollItems.each(function(el) {
    if(el.href == window.location.hash) {
      $('body, html').scrollTop(0);
    }
  });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  $('a[data-animate]').on('click', function(e) {
    var anchor = $(this).data('animate');
    var $target = $(anchor);
    if (anchor.length && $target.length) {
      e.preventDefault();
      var offsetTop = page.calculateOffsetToElem($target);
      $('html, body').animate({
        scrollTop: offsetTop
      }, 300);
    }
  });

  var navbarCollapse = $('.navbar-collapse').is(':visible'),
      navBar = $('.navbar');

  $('.dropdown ul a').click(function(e){
    var href = $(this).attr('href'),
      offsetTop = href === '#' ? 0 : $(href).offset().top-topMenuHeight+1;

    if ($('.navbar-toggle').is(':visible')) {
      $('.navbar-toggle').click();
    }

    $('html, body').stop().animate({
      scrollTop: offsetTop
    }, 300);
    window.location.hash = href;
    e.preventDefault();
  });

  // navbar
  var thisHeight;
  $('.navbar-collapse').on('shown.bs.collapse', function () {
    thisHeight = $(this).outerHeight() > $(window).height();
    if (thisHeight) {
      navBar.addClass('navbar-scroll');
    }
  });

  $('.navbar-collapse').on('hide.bs.collapse', function () {
    navBar.removeClass('navbar-scroll');
  });

  // open news by hash tag
  var urlAnchor = document.location.hash;
  var newsCtrl = {};
  newsCtrl.$newsList = $('#news-list');
  newsCtrl.anchors = [].slice.call(($('#news-list [id^=news]') || []).map(function(_, el){
    return el.id;
  }), 0);

  /**
   * Check is hash belongs to news list.
   * @param anchor {String} - params match to string like '#<some-text>',
   * in other word with hash forward
   * @returns {Boolean}
   */
  newsCtrl.isAnchorBelongs = function(anchor){
    return this.anchors.indexOf(anchor.slice(1)) != -1;
  };

  console.log(newsCtrl.anchors);
  //do it only after the DOM ready
  $(document).ready(function(){
    if (urlAnchor) {
      var $hashCollapse = $(urlAnchor);
      var prevEl = 0;

      if(newsCtrl.isAnchorBelongs(urlAnchor)){
        prevEl = ($hashCollapse.prev()) ? $hashCollapse.prev().outerHeight() : 0;
        prevEl += 20;
      }

      setTimeout(function() {
        if (newsCtrl.isAnchorBelongs(urlAnchor)){
          $hashCollapse.addClass('in');
        }
        var sum = page.calculateOffsetToElem($hashCollapse) - prevEl;

        $('body, html').animate({
          scrollTop: sum
        }, 400);
      }, 400);//add time out to let some async func became done.(dirty hack)
    }
  });


  // Bind to scroll
  $(window).scroll(function(){
    // Get container scroll position
    var navBarTo = ($('.navbar-header').length) ? $('.navbar-header').outerHeight() : 0;
    var dropdown = ($('.dropdown').length) ? $('.dropdown').outerHeight() : 0;
    var fromTop = $(this).scrollTop() + navBarTo + dropdown;
    // Get id of current scroll item
    var cur = scrollItems.map(function(){
      if ($(this).offset().top < fromTop)
        return this;
    });
    // Get the id of the current element
    cur = cur[cur.length-1];
    var id = cur && cur.length ? cur[0].id : '';

    if (lastId !== id) {
        lastId = id;
        menuItems
          .parent().removeClass('active')
          .end().filter('[href=\'#'+id+'\']').parent().addClass('active');
    }
  });

  function centerModal() {
    $(this).css('display', 'block');
    var $dialog  = $(this).find('.modal-dialog'),
        offsetY = ($(window).height() - $dialog.height()) / 2,
        offsetX = ($(window).width() - $dialog.width()) / 2,
        bottomMargin = parseInt($dialog.css('marginBottom'), 10);

    if(offsetY < bottomMargin) offsetY = bottomMargin;
    $dialog.css({
      'margin-top': offsetY,
      'margin-right': offsetX,
      'margin-left': offsetX
    });
  }

  $(document).on('show.bs.modal', '.modal', centerModal);
  $(window).on('resize', function () {
      $('.modal:visible').each(centerModal);
  });

  // open modal window by hash inside URL
  var target = document.location.hash.replace('#', '');
  if (target.length) {
    if (target === 'start') {
      var begin = $('#begin').offset().top;
      $(window).scrollTop(begin);
      $('#'+target).modal('show');
    }
  }


  var navDrop = $('.navbar-nav2'),
      navAuth = $('.nav-auth'),
      navbarCollapse = $('.navbar-collapse');

  function autoClickNav() {
    var wWidth = $(window).width();

    if (wWidth < 992) {
      navAuth.addClass('notouch');
    } else {
      navAuth.removeClass('notouch');
    }
  }
  autoClickNav();
  $(window).resize(autoClickNav);
  navDrop.on('click', '.nav-auth', function(e) {
    var $that = $(this);

    if (navAuth.hasClass('notouch')) {
      if (navbarCollapse.hasClass('in')) {
        $('.navbar-toggle').click();
      }
      if ($that.hasClass('open')) {
        $that.toggleClass('open');
      } else {
        navAuth.removeClass('open');
        $that.addClass('open');
      }
    }
  });

  $('.navbar-collapse').on('shown.bs.collapse', function () {
    $('.nav-auth').removeClass('open');
  });



})(jQuery, window);